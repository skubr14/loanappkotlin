package com.twino.loanapplication.repository

import com.twino.loanapplication.model.RefreshToken
import org.springframework.data.jpa.repository.JpaRepository

interface RefreshTokenRepository : JpaRepository<RefreshToken, Long> {
	fun getByToken(token: String): RefreshToken
	fun removeByToken(token: String)
	fun existsByToken(token: String): Boolean
}