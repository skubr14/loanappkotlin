package com.twino.loanapplication.repository

import com.twino.loanapplication.model.UserRole
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface RoleRepository: JpaRepository<UserRole, Long>{
	fun findByName(name: String) : UserRole?
	fun getByName(name: String) : UserRole
}