package com.twino.loanapplication.repository

import com.twino.loanapplication.model.User
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface UserRepository : JpaRepository<User, Long> {
	fun getByUsername(username: String): User
}


