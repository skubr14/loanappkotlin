package com.twino.loanapplication.repository

import org.springframework.data.jpa.repository.JpaRepository
import com.twino.loanapplication.model.LoanApplication
import org.springframework.stereotype.Repository

@Repository
interface LoanApplicationRepository : JpaRepository<LoanApplication, Long>