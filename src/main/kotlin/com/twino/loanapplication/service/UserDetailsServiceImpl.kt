package com.twino.loanapplication.service

import com.twino.loanapplication.repository.UserRepository
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service(value = "UserDetailsServiceImpl")
class UserDetailsServiceImpl(private val userRepository: UserRepository) : UserDetailsService {

	@Transactional(readOnly = true)
	override fun loadUserByUsername(username: String): UserDetails? {
		val user = userRepository.getByUsername(username)
		val roles = user.roles?.map{ SimpleGrantedAuthority(it.name) }
		return org.springframework.security
			.core.userdetails.User(user.username, user.password,
				true, true, true,
				true, roles)
	}
}
