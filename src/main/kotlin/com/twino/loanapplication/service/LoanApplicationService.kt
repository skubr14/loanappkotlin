package com.twino.loanapplication.service

import com.twino.loanapplication.dto.CreateLoanAppRequest
import com.twino.loanapplication.dto.LoanApplicationDto
import com.twino.loanapplication.model.LoanApplication
import com.twino.loanapplication.repository.LoanApplicationRepository
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Sort
import org.springframework.security.access.annotation.Secured
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.math.BigDecimal
import java.time.ZoneOffset
import java.time.ZonedDateTime

@Service
class LoanApplicationService(private val loanApplicationRepository: LoanApplicationRepository) {

	enum class LoanStatus {
		REJECTED,
		APPROVED,
		MANUAL
	}

	@Transactional // TODO: what does Transactional annotation do
	fun save(createLoanAppRequest: CreateLoanAppRequest): LoanApplicationDto {
		val loanApplication = LoanApplication(
			createLoanAppRequest.personalId,
			createLoanAppRequest.firstName,
			createLoanAppRequest.lastName,
			createLoanAppRequest.birthDate,
			createLoanAppRequest.employer,
			createLoanAppRequest.salary,
			createLoanAppRequest.monthlyLiability,
			createLoanAppRequest.requestedAmount,
			createLoanAppRequest.requestedTerm
		)
		val score = calculateScore(loanApplication)
		loanApplication.status = resolveStatusBy(score)
		loanApplicationRepository.save(loanApplication)
		return mapToDto(loanApplication)
	}

	@Secured("ROLE_ADMIN")
	@Transactional
	fun update(loanApplicationId: Long, status: LoanStatus): LoanApplicationDto {
		val application: LoanApplication = loanApplicationRepository.getOne(loanApplicationId)
		application.status = status
		loanApplicationRepository.save(application)
		return mapToDto(application)
	}

	fun calculateScore(loanApplication: LoanApplication): BigDecimal {
		var score: BigDecimal = 0.0.toBigDecimal()
		val firstName: String = loanApplication.firstName.toLowerCase()
		score += firstName.map(Char::toInt).reduce { sumOfNameChars, i -> sumOfNameChars + i - 'a'.toInt() }
			.toBigDecimal()
		score += loanApplication.salary * 1.5.toBigDecimal()
		score -= loanApplication.monthlyLiability * 3.toBigDecimal()
		val birthDate: ZonedDateTime = loanApplication.birthDate.atZone(ZoneOffset.UTC)
		val birthYear = birthDate.year
		score += birthYear.toBigDecimal()
		val monthOfBirth = birthDate.monthValue
		score -= monthOfBirth.toBigDecimal()
		val julianDay = birthDate.dayOfYear
		score -= julianDay.toBigDecimal()

		return score
	}

	fun resolveStatusBy(score: BigDecimal): LoanStatus =
		when {
			score < 2500.toBigDecimal() -> {
				LoanStatus.REJECTED
			}
			score > 3500.toBigDecimal() -> {
				LoanStatus.APPROVED
			}
			else -> {
				LoanStatus.MANUAL
			}
		}

	@Transactional(readOnly = true) // TODO: add rules in editor (see preferences) set ruler 80, 100 (Kotlin), 120 (Java)
	fun getAll(columnName: String, direction: String, page: Int, pageSize: Int): List<LoanApplicationDto> {
		return loanApplicationRepository
			.findAll(
				PageRequest.of(page, pageSize, Sort.by(Sort.Direction.fromString(direction), columnName))
			).content
			.map(this::mapToDto)
	} // TODO: PaginatedResult<LoanApplicationDto> (content, page = 2, size = 12, totalSize = 400)

	@Transactional(readOnly = true)
	fun getManualApplications(): List<LoanApplicationDto> {
		return loanApplicationRepository.findAll()
			.map(this::mapToDto)
			.filter { loanApplication -> loanApplication.status == LoanStatus.MANUAL }
	}

	private fun mapToDto(loanApplication: LoanApplication): LoanApplicationDto {
		return LoanApplicationDto(loanApplication.id,
			loanApplication.personalId,
			loanApplication.firstName,
			loanApplication.lastName,
			loanApplication.birthDate,
			loanApplication.employer,
			loanApplication.salary,
			loanApplication.monthlyLiability,
			loanApplication.requestedAmount,
			loanApplication.requestedTerm,
			loanApplication.status)
	}

	@Secured("ROLE_ADMIN")
	@Transactional
	fun delete(loanApplicationId: Long) {
		loanApplicationRepository.deleteById(loanApplicationId)
	}
}