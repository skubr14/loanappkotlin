package com.twino.loanapplication.service

import org.springframework.security.core.Authentication
import com.twino.loanapplication.dto.AuthenticationResponse
import com.twino.loanapplication.dto.LoginRequest
import com.twino.loanapplication.dto.RefreshTokenRequest
import com.twino.loanapplication.dto.RegisterRequest
import com.twino.loanapplication.logging.LoggerDelegate
import com.twino.loanapplication.model.User
import com.twino.loanapplication.model.UserRole
import com.twino.loanapplication.repository.RoleRepository
import com.twino.loanapplication.repository.UserRepository
import com.twino.loanapplication.security.JwtProvider
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.time.Instant

@Service
@Transactional
class AuthService(
	private val userRepository: UserRepository,
	private val passwordEncoder: PasswordEncoder,
	private val authenticationManager: AuthenticationManager,
	private val jwtProvider: JwtProvider,
	private val refreshTokenService: RefreshTokenService,
	private val roleRepository: RoleRepository,
) {

	private val logger by LoggerDelegate()

	fun signup(registerRequest: RegisterRequest) {

		val user = User(registerRequest.username,
			passwordEncoder.encode(registerRequest.password),
			registerRequest.email,
			Instant.now(),
			registerRequest.isOperator)
		val name = if (user.isOperator) "ROLE_ADMIN" else "ROLE_USER"
		setupRoles()
		val role = roleRepository.getByName(name)
		user.roles = mutableSetOf(role)
		logger.info(user.username)
		userRepository.save(user)
	}

	fun logout(refreshToken: String) {
		refreshTokenService.deleteRefreshToken(refreshToken)
	}

	fun login(loginRequest: LoginRequest): AuthenticationResponse {

		val credentials = UsernamePasswordAuthenticationToken(loginRequest.username,
			loginRequest.password)

		val authenticate: Authentication =
			authenticationManager.authenticate(credentials)

		SecurityContextHolder.getContext().authentication = authenticate
		val token = jwtProvider.generateToken(authenticate)
		return AuthenticationResponse(token,
			refreshTokenService.generateRefreshToken().token,
			Instant.now().plusMillis(jwtProvider.getJwtExpirationInMillis()),
			loginRequest.username)
	}

	fun refreshToken(refreshTokenRequest: RefreshTokenRequest): AuthenticationResponse {
		refreshTokenService.validateRefreshToken(refreshTokenRequest.refreshToken)
		val token = jwtProvider.generateTokenWithUserName(refreshTokenRequest.username)
		return AuthenticationResponse(
			token,
			refreshTokenRequest.refreshToken,
			Instant.now().plusMillis(jwtProvider.getJwtExpirationInMillis()),
			refreshTokenRequest.username
		)
	}

	private fun setupRoles() {
		createRoleIfNotFound("ROLE_ADMIN")
		createRoleIfNotFound("ROLE_USER")
	}

	@Transactional
	fun createRoleIfNotFound(
		name: String,
	) {
		if (roleRepository.findByName(name) == null) {
			roleRepository.save<UserRole>(UserRole(name))
		}
	}
}