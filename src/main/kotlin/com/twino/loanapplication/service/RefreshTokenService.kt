package com.twino.loanapplication.service

import com.twino.loanapplication.model.RefreshToken
import com.twino.loanapplication.repository.RefreshTokenRepository
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.time.Instant
import java.util.*

@Service
class RefreshTokenService(private val refreshTokenRepository: RefreshTokenRepository) {

	@Transactional
	fun generateRefreshToken(): RefreshToken {
		val refreshToken = RefreshToken(UUID.randomUUID().toString(), Instant.now())
		return refreshTokenRepository.save(refreshToken)
	}

	@Transactional(readOnly = true)
	fun validateRefreshToken(token: String) {
		refreshTokenRepository.getByToken(token)
	}

	@Transactional
	fun deleteRefreshToken(token: String) {
		refreshTokenRepository.removeByToken(token)
	}
}
