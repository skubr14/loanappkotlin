package com.twino.loanapplication.controller

import com.twino.loanapplication.dto.CreateLoanAppRequest
import com.twino.loanapplication.dto.LoanApplicationDto
import com.twino.loanapplication.dto.UpdateStatusRequest
import com.twino.loanapplication.service.LoanApplicationService
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/api/loan-application")
class LoanApplicationController(private val loanApplicationService: LoanApplicationService) {

	@PostMapping
	fun createLoanApplication(@RequestBody createLoanAppRequest: CreateLoanAppRequest): ResponseEntity<LoanApplicationDto> {
		return ResponseEntity(loanApplicationService.save(createLoanAppRequest), HttpStatus.CREATED)
	}

	@PostMapping("/edit/{id}")
	fun editLoanApplication(
		@PathVariable("id") loanApplicationId: Long, @RequestBody updateStatusRequest: UpdateStatusRequest,
	): ResponseEntity<LoanApplicationDto> {
		return ResponseEntity.status(HttpStatus.OK)
			.body(loanApplicationService.update(loanApplicationId, updateStatusRequest.status))
	}

	@GetMapping("/manualApps")
	fun getAllManualApplications(): ResponseEntity<List<LoanApplicationDto>> {
		return ResponseEntity
			.status(HttpStatus.OK)
			.body(loanApplicationService.getManualApplications())
	}

	@GetMapping
	fun getAllLoanApplications(
		@RequestParam(defaultValue = "id") sort: String,
		@RequestParam(defaultValue = "ASC") direction: String,
		@RequestParam(defaultValue = "0") page: Int,
		@RequestParam(defaultValue = "10") pageSize: Int,
	): ResponseEntity<List<LoanApplicationDto>> {
		return ResponseEntity
			.status(HttpStatus.OK)
			.body(loanApplicationService.getAll(sort, direction, page, pageSize))
	}

	@DeleteMapping("/{id}")
	private fun deleteLoanApplication(@PathVariable("id") loanApplicationId: Long) {
		loanApplicationService.delete(loanApplicationId)
	}

}