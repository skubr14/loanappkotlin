package com.twino.loanapplication.controller

import com.twino.loanapplication.dto.*
import com.twino.loanapplication.service.AuthService
import com.twino.loanapplication.service.RefreshTokenService
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import javax.security.auth.callback.ConfirmationCallback.OK
import javax.validation.Valid

@RestController
@RequestMapping("/api/auth")
class AuthController(
	private val authService: AuthService
) {

	@PostMapping("/signup")
	fun signup(@RequestBody registerRequest: RegisterRequest): ResponseEntity<String> {
		authService.signup(registerRequest)
		return ResponseEntity("User Registration Successful", HttpStatus.OK)
	}

	@PostMapping("/login")
	fun login(@RequestBody loginRequest: LoginRequest): AuthenticationResponse {
		return authService.login(loginRequest)
	}

	@PostMapping("/refresh/token")
	fun refreshTokens(@Valid @RequestBody refreshTokenRequest: RefreshTokenRequest): AuthenticationResponse {
		return authService.refreshToken(refreshTokenRequest)
	}

	@PostMapping("/logout")
	fun logout(@Valid @RequestBody logoutRequest: LogoutRequest): ResponseEntity<String> {
		authService.logout(logoutRequest.refreshToken)
		return ResponseEntity("User logged out", HttpStatus.OK)
	}
}
