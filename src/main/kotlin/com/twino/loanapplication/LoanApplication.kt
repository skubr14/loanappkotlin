package com.twino.loanapplication

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean
import org.springframework.web.context.request.RequestContextListener




@SpringBootApplication
class LoanApplication

fun main(args: Array<String>) {
	runApplication<LoanApplication>(*args)
}

@Bean
fun requestContextListener(): RequestContextListener {
	return RequestContextListener()
}
