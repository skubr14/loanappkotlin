package com.twino.loanapplication.config

import com.twino.loanapplication.security.JwtAuthenticationFilter
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.context.annotation.Bean
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.config.BeanIds
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter

@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true)
class SecurityConfig(
	@Qualifier("UserDetailsServiceImpl") private val userDetailsService: UserDetailsService,
	private val jwtAuthenticationFilter: JwtAuthenticationFilter,
) : WebSecurityConfigurerAdapter() {

	@Bean(BeanIds.AUTHENTICATION_MANAGER)
	override fun authenticationManagerBean(): AuthenticationManager = super.authenticationManagerBean()

	override fun configure(httpSecurity: HttpSecurity) {
		httpSecurity.cors().and()
			.csrf().disable() // TODO: what is CORS / CSRF
			.authorizeRequests()
			.antMatchers("/api/auth/**").permitAll()
			.anyRequest()
			.authenticated()
			.and()
			.addFilterBefore(jwtAuthenticationFilter,
				UsernamePasswordAuthenticationFilter::class.java)
	}

	@Autowired
	fun configureGlobal(authenticationManagerBuilder: AuthenticationManagerBuilder) {
		authenticationManagerBuilder.userDetailsService(userDetailsService)
			.passwordEncoder(passwordEncoder())
	}

	@Bean
	fun passwordEncoder() = BCryptPasswordEncoder()

}