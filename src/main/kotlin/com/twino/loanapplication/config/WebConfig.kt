package com.twino.loanapplication.config

import org.springframework.context.annotation.Configuration
import org.springframework.web.servlet.config.annotation.CorsRegistry
import org.springframework.web.servlet.config.annotation.EnableWebMvc
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer

@Configuration
@EnableWebMvc
class WebConfig : WebMvcConfigurer {

	override fun addCorsMappings(corsRegistry: CorsRegistry) { // TODO: explain what this is doing
		corsRegistry.addMapping("/**")
			.allowedOrigins("*")
			.allowedOriginPatterns("*")
			.allowedMethods("*")
			.maxAge(3600L)
			.allowedHeaders("*")
			.exposedHeaders("Authorization")
//			.allowCredentials(true)
	}
}
