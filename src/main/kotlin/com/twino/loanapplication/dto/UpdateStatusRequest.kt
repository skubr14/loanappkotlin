package com.twino.loanapplication.dto

import com.twino.loanapplication.service.LoanApplicationService
import javax.validation.constraints.NotBlank

data class UpdateStatusRequest(
	@NotBlank val status: LoanApplicationService.LoanStatus,
)