package com.twino.loanapplication.dto

data class LoginRequest(val username: String, val password: String)
