package com.twino.loanapplication.dto

import com.twino.loanapplication.service.LoanApplicationService.LoanStatus
import java.math.BigDecimal
import java.time.Instant

data class LoanApplicationDto(
	val id: Long,
	val personalId: String,
	val firstName: String,
	val lastName: String,
	val birthDate: Instant,
	val employer: String,
	val salary: BigDecimal,
	val monthlyLiability: BigDecimal,
	val requestedAmount: BigDecimal,
	val requestedTerm: String,
	val status: LoanStatus,
)
