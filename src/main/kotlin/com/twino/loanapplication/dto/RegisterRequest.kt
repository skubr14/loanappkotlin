package com.twino.loanapplication.dto

import javax.validation.constraints.Email
import javax.validation.constraints.NotBlank

data class RegisterRequest(
	@Email
	@NotBlank(message = "Email is required")
	val email: String,
	@NotBlank(message = "Username is required")
	val username: String,
	@NotBlank(message = "Password is required")
	val password: String,

	val isOperator: Boolean,
)

