package com.twino.loanapplication.dto

import org.springframework.lang.NonNull
import java.math.BigDecimal
import java.time.Instant

data class CreateLoanAppRequest(
	@NonNull val personalId: String,
	@NonNull val firstName: String,
	@NonNull val lastName: String,
	@NonNull val birthDate: Instant,
	@NonNull val employer: String,
	@NonNull val salary: BigDecimal,
	@NonNull val monthlyLiability: BigDecimal,
	@NonNull val requestedAmount: BigDecimal,
	@NonNull val requestedTerm: String,
)
