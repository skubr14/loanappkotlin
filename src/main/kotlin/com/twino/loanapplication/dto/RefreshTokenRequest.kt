package com.twino.loanapplication.dto

import javax.validation.constraints.NotBlank

data class RefreshTokenRequest(
	@NotBlank
	val refreshToken: String,
	val username: String,
)
