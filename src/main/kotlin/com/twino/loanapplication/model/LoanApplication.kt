package com.twino.loanapplication.model

import com.twino.loanapplication.service.LoanApplicationService.LoanStatus
import java.math.BigDecimal
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import java.time.Instant
import javax.persistence.Column

@Entity
class LoanApplication(
	@Column(name = "personal_id", nullable = false) // Hibernate validation
	val personalId: String,
	@Column(name = "firstname", nullable = false)
	val firstName: String,
	@Column(name = "lastname", nullable = false)
	val lastName: String,
	@Column(name = "birth_date", nullable = false)
	val birthDate: Instant,
	@Column(name = "employer", nullable = false)
	var employer: String,
	@Column(name = "salary", nullable = false)
	var salary: BigDecimal,
	@Column(name = "liability", nullable = false)
	var monthlyLiability: BigDecimal,
	@Column(name = "requested_amount", nullable = false)
	var requestedAmount: BigDecimal,
	@Column(name = "requested_term", nullable = false)
	var requestedTerm: String,
	@Column(name = "status", nullable = false)
	var status: LoanStatus = LoanStatus.MANUAL,
	@Id @GeneratedValue var id: Long = 0,
)