package com.twino.loanapplication.model

import java.time.Instant
import javax.persistence.*

import javax.persistence.GenerationType.IDENTITY

@Entity
class User(
	@Column(name = "username", nullable = false)
	var username: String,
	@Column(name = "password", nullable = false)
	var password: String,
	@Column(name = "email", nullable = false)
	var email: String,
	@Column(name = "created_at", nullable = false)
	val created: Instant = Instant.now(),
	var isOperator: Boolean = false,
	@JoinTable
	@OneToMany(fetch = FetchType.EAGER)
	var roles: MutableSet<UserRole>? = null,
	@Id
	@GeneratedValue(strategy = IDENTITY)
	var id: Long? = null,
)

@Entity
class UserRole(
	var name: String, // EDITOR, ADMIN, CLIENT...
	@Id
	@GeneratedValue(strategy = IDENTITY)
	var id: Long? = null,
)