package com.twino.loanapplication.model

import java.time.Instant
import javax.persistence.*

@Entity
class RefreshToken(
	@Column(name = "token", nullable = false)
	var token: String,
	@Column(name = "created_at", nullable = false)
	var createdDate: Instant,
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	val id: Long = 0,
)

