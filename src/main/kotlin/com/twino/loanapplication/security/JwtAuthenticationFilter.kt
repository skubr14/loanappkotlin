package com.twino.loanapplication.security

import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource
import org.springframework.stereotype.Component
import org.springframework.util.StringUtils
import org.springframework.web.filter.OncePerRequestFilter
import java.io.IOException
import javax.servlet.FilterChain
import javax.servlet.ServletException
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Component
class JwtAuthenticationFilter(
	private val jwtProvider: JwtProvider,
	@Qualifier("UserDetailsServiceImpl")
	private val userDetailsService: UserDetailsService,
) : OncePerRequestFilter() {

	companion object {
		const val HEADER_NAME = "Authorization"
		const val TOKEN_PREFIX = "Bearer "
		val EXCLUDE_URL = listOf("/auth/")
	}

	@Throws(ServletException::class, IOException::class)
	override fun doFilterInternal(
		request: HttpServletRequest,
		response: HttpServletResponse,
		filterChain: FilterChain,
	) {
		val jwt = getJwtFromRequest(request)
		val username = jwtProvider.getUsernameFromJwt(jwt)
		if (StringUtils.hasText(jwt) && username != null) {
			val userDetails = userDetailsService.loadUserByUsername(username)
			val authentication = UsernamePasswordAuthenticationToken(userDetails,
				null, userDetails.authorities)
			authentication.details = WebAuthenticationDetailsSource().buildDetails(request)
			SecurityContextHolder.getContext().authentication = authentication
		}
		filterChain.doFilter(request, response)

	}

	override fun shouldNotFilter(request: HttpServletRequest): Boolean {
		return EXCLUDE_URL.any { request.requestURI.contains(it) }
	}

	private fun getJwtFromRequest(request: HttpServletRequest): String {
		val bearerToken = request.getHeader(HEADER_NAME)
		return if (StringUtils.hasText(bearerToken) && bearerToken.startsWith(TOKEN_PREFIX)) {
			bearerToken.substring(7)
		} else bearerToken
	}
}