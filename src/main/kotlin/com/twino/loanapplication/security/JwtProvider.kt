package com.twino.loanapplication.security

import io.jsonwebtoken.Jwts
import io.jsonwebtoken.Jwts.parser
import java.sql.Date
import java.time.Instant
import org.springframework.beans.factory.annotation.Value
import org.springframework.security.core.Authentication
import org.springframework.security.core.userdetails.User
import org.springframework.stereotype.Service
import java.io.InputStream
import java.security.KeyStore
import java.security.KeyStoreException
import java.security.PrivateKey
import java.security.PublicKey
import java.util.Date.from
import javax.annotation.PostConstruct

@Service
class JwtProvider {

	private lateinit var keyStore: KeyStore

	@Value("\${jwt.expiration.time}")
	private var jwtExpirationInMillis: Long = 0

	@PostConstruct
	fun init() {
		try {
			keyStore = KeyStore.getInstance("JKS")
			val resourceAsStream: InputStream = this::class.java.getResourceAsStream("/springblog.jks")
			keyStore.load(resourceAsStream, "secret".toCharArray())
		} catch (e: Exception) {
			throw Exception("Exception occurred while loading keystore", e)
		}
	}

	fun generateToken(authentication: Authentication): String {
		val principal: User = authentication.principal as User
		val username = principal.username
		return generateTokenWithUserName(username)
	}

	fun generateTokenWithUserName(username: String): String {
		return Jwts.builder()
			.setSubject(username)
			.setIssuedAt(from(Instant.now()))
			.signWith(getPrivateKey())
			.setExpiration(Date.from(Instant.now().plusMillis(jwtExpirationInMillis)))
			.compact()
	}

	private fun getPrivateKey(): PrivateKey {
		try {
			return keyStore.getKey("springblog", "secret".toCharArray()) as PrivateKey
		} catch (e: Exception) {
			throw Exception("Exception occurred while retrieving public key from keystore", e)
		}
	}

	private fun getPublicKey(): PublicKey {
		try {
			return keyStore.getCertificate("springblog").publicKey
		} catch (e: KeyStoreException) {
			throw Exception("Exception occurred while retrieving public key from keystore", e)
		}
	}

	fun getUsernameFromJwt(jwt: String): String? {
		return try {
			val claims = parser()
				.setSigningKey(getPublicKey())
				.parseClaimsJws(jwt).body
			return if (claims.expiration != null && claims.expiration.before(Date.from(Instant.now()))){
				null
			} else claims.subject
		} catch (e: Exception) {
			null
		}
	}

	fun getJwtExpirationInMillis(): Long {
		return jwtExpirationInMillis
	}
}
