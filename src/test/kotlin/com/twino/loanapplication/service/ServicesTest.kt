package com.twino.loanapplication.service

import com.twino.loanapplication.dto.*
import com.twino.loanapplication.repository.LoanApplicationRepository
import com.twino.loanapplication.repository.RefreshTokenRepository
import com.twino.loanapplication.repository.UserRepository
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.annotation.DirtiesContext
import java.time.Instant

@SpringBootTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.ANY)
class ServicesTest {

	@Autowired
	lateinit var loanApplicationService: LoanApplicationService

	@Autowired
	lateinit var authService: AuthService

	@Autowired
	lateinit var userRepository: UserRepository

	@Autowired
	lateinit var refreshTokenRepository: RefreshTokenRepository

	@Autowired
	lateinit var loanApplicationRepository: LoanApplicationRepository

	@BeforeEach
	fun setup() {
		performSignup(RegisterRequest("test@gmail.com", "test", "pass", true))
		val token = getAuth().refreshToken
		performCreateLoanApplication(CreateLoanAppRequest(
			"personal Id",
			"firstname",
			"lastname",
			Instant.ofEpochMilli(977577769000),
			"employer",
			5000.0.toBigDecimal(),
			1000.0.toBigDecimal(),
			50000.0.toBigDecimal(),
			"month"),
			token
		)
		performCreateLoanApplication(CreateLoanAppRequest(
			"personal Id",
			"firstname",
			"lastname",
			Instant.ofEpochMilli(977577769000),
			"employer",
			1000.0.toBigDecimal(),
			500.0.toBigDecimal(),
			50000.0.toBigDecimal(),
			"month"),
			token
		)
		performCreateLoanApplication(CreateLoanAppRequest(
			"personal Id",
			"firstname",
			"lastname",
			Instant.ofEpochMilli(977577769000),
			"employer",
			800.0.toBigDecimal(),
			200.0.toBigDecimal(),
			50000.0.toBigDecimal(),
			"month"),
			token
		)

	}

	private fun performSignup(registerRequest: RegisterRequest) {
		authService.signup(registerRequest)
	}

	@Test
	fun signupAndLogin() {
		val registerRequest = RegisterRequest(
			"test@gmail.com",
			"user",
			"pass",
			false
		)
		performSignup(registerRequest)
		val user = userRepository.getByUsername(registerRequest.username)
		assert(user.username == registerRequest.username)
		assert(user.email == registerRequest.email)
		assert(user.roles?.size == 1)
		assert(user.roles?.elementAt(0)?.name == "ROLE_USER")

	}

	@Test
	fun login() {
		val loginResponse = authService.login(LoginRequest("test", "pass"))
		assert(loginResponse.username == "test")
	}

	@Test
	fun refreshTokens() {
		val token = getAuth().refreshToken
		val response = authService.refreshToken(RefreshTokenRequest(token, "test"))
		assert(response.refreshToken == token)
	}

	private fun getAuth() = authService.login(LoginRequest("test", "pass"))


	@Test
	fun logout() {
		val token = getAuth().refreshToken
		authService.logout(token)
		assert(!refreshTokenRepository.existsByToken(token))
	}

	@Test
	fun createLoanApplication() {
		val token = getAuth().authenticationToken
		doTest(CreateLoanAppRequest(
			"personal Id",
			"firstname",
			"lastname",
			Instant.ofEpochMilli(977577769000),
			"employer",
			5000.0.toBigDecimal(),
			1000.0.toBigDecimal(),
			50000.0.toBigDecimal(),
			"month"), token, LoanApplicationService.LoanStatus.APPROVED)

		doTest(CreateLoanAppRequest(
			"personal ID",
			"first",
			"last",
			Instant.ofEpochMilli(977577769000),
			"employer",
			1000.0.toBigDecimal(),
			500.0.toBigDecimal(),
			50000.0.toBigDecimal(),
			"month"), token, LoanApplicationService.LoanStatus.REJECTED)

	}

	private fun doTest(
		createLoanAppRequest: CreateLoanAppRequest,
		token: String,
		status: LoanApplicationService.LoanStatus,
	) {
		val loanResponse = performCreateLoanApplication(createLoanAppRequest, token)
		assert(loanApplicationRepository.existsById(loanResponse.id))
		assert(loanResponse.status == status)
		assert(loanResponse.firstName == createLoanAppRequest.firstName)
		assert(loanResponse.lastName == createLoanAppRequest.lastName)
		assert(loanResponse.personalId == createLoanAppRequest.personalId)
		assert(loanResponse.monthlyLiability == createLoanAppRequest.monthlyLiability)
	}

	fun performCreateLoanApplication(
		createLoanAppRequest: CreateLoanAppRequest,
		authenticationToken: String,
	) = loanApplicationService.save(createLoanAppRequest)


	@Test
	fun editLoanApplication() {
		val token = getAuth().authenticationToken
		val loanApplication = CreateLoanAppRequest(
			"personal Id",
			"firstname",
			"lastname",
			Instant.ofEpochMilli(977577769000),
			"employer",
			800.0.toBigDecimal(),
			200.0.toBigDecimal(),
			50000.0.toBigDecimal(),
			"month")
		val loanResponse = performCreateLoanApplication(loanApplication, token)
		val id: Long = loanResponse.id
		val editResponse = loanApplicationService.update(id, LoanApplicationService.LoanStatus.APPROVED)
		assert(editResponse.status == LoanApplicationService.LoanStatus.APPROVED)
		assert(editResponse.status == loanApplicationRepository.getOne(id).status)
	}

	@Test
	fun getAllManualApplications() {
		val manualApps = loanApplicationService.getManualApplications()
		assert(manualApps.all { it.status == LoanApplicationService.LoanStatus.MANUAL })
	}

	@Test
	fun getAllLoanApplications() {
		val allLoanApplications = loanApplicationService.getAll("firstName", "ASC", 0, 10)
		assert(allLoanApplications.size == 3)
	}

	@Test
	fun deleteLoanApplication() {
		val token = getAuth().authenticationToken
		val loanResponse = performCreateLoanApplication(CreateLoanAppRequest(
			"personal Id",
			"firstname",
			"lastname",
			Instant.ofEpochMilli(977577769000),
			"employer",
			800.0.toBigDecimal(),
			200.0.toBigDecimal(),
			50000.0.toBigDecimal(),
			"month"), token)
		val id = loanResponse.id
		loanApplicationService.delete(id)
		assert(!loanApplicationRepository.existsById(id))
	}
}