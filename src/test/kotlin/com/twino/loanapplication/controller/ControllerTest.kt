package com.twino.loanapplication.controller

import com.fasterxml.jackson.databind.ObjectMapper
import com.twino.loanapplication.dto.*
import com.twino.loanapplication.model.LoanApplication
import com.twino.loanapplication.repository.LoanApplicationRepository
import com.twino.loanapplication.service.LoanApplicationService
import org.hamcrest.Matchers.`is`
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.mockito.BDDMockito.given
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.data.domain.PageImpl
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Sort
import org.springframework.http.MediaType
import org.springframework.test.annotation.DirtiesContext
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.ResultActions
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import java.time.Instant

@SpringBootTest
@AutoConfigureMockMvc
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.ANY)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
internal class ControllerTest {

	@Autowired
	lateinit var mapper: ObjectMapper

	@Autowired
	lateinit var mockMvc: MockMvc

	@MockBean
	lateinit var loanApplicationRepository: LoanApplicationRepository

	@BeforeEach
	fun setup() {
		performSignup("test@gmail.com", "test", "pass", true)
		given(loanApplicationRepository.findAll()).willReturn(listOf(
			LoanApplication(
				"",
				"",
				"",
				Instant.ofEpochMilli(977577769000),
				"employer",
				800.0.toBigDecimal(),
				200.0.toBigDecimal(),
				50000.0.toBigDecimal(),
				"month", LoanApplicationService.LoanStatus.MANUAL, 0
			),
			LoanApplication(
				"",
				"",
				"",
				Instant.ofEpochMilli(977577769000),
				"employer",
				800.0.toBigDecimal(),
				200.0.toBigDecimal(),
				50000.0.toBigDecimal(),
				"month", LoanApplicationService.LoanStatus.REJECTED, 0
			)))
		given(loanApplicationRepository.findAll(PageRequest.of(0,
			10,
			Sort.by(Sort.Direction.fromString("ASC"), "firstName")))).willReturn(PageImpl(listOf(
			LoanApplication(
				"",
				"A",
				"",
				Instant.ofEpochMilli(977577769000),
				"employer",
				800.0.toBigDecimal(),
				200.0.toBigDecimal(),
				50000.0.toBigDecimal(),
				"month", LoanApplicationService.LoanStatus.MANUAL, 0
			),
			LoanApplication(
				"",
				"B",
				"",
				Instant.ofEpochMilli(977577769000),
				"employer",
				800.0.toBigDecimal(),
				200.0.toBigDecimal(),
				50000.0.toBigDecimal(),
				"month", LoanApplicationService.LoanStatus.REJECTED, 0
			))))
	}

	@Test
	fun signup() {
		performSignup("test@gmail.com", "user", "pass", false)
			.andExpect(status().isOk)
			.andReturn()
	}

	fun performSignup(email: String, username: String, password: String, isOperator: Boolean): ResultActions {
		val request = MockMvcRequestBuilders.post("/api/auth/signup").contentType(MediaType.APPLICATION_JSON)
			.content(mapper.writeValueAsString(RegisterRequest(email, username, password, isOperator)))
		return mockMvc.perform(request)
	}

	@Test
	fun login() {
		performLogin("test", "pass")
			.andExpect(status().isOk)
			.andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON))
			.andExpect(MockMvcResultMatchers.jsonPath("\$.username").value("test"))
	}

	fun performLogin(username: String, password: String): ResultActions {
		return mockMvc.perform(MockMvcRequestBuilders.post("/api/auth/login").contentType(MediaType.APPLICATION_JSON)
			.content(mapper.writeValueAsString(LoginRequest(username, password))))
	}

	@Test
	fun refreshTokens() {
		val token = getAuth().refreshToken
		mockMvc
			.perform(MockMvcRequestBuilders
				.post("/api/auth/refresh/token")
				.contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(RefreshTokenRequest(token,
					"test"))))
			.andExpect(status().isOk)
	}

	private fun getAuth(username: String = "test", password: String = "pass"): AuthenticationResponse {
		val loginResponse = performLogin(username, password).andReturn().response.contentAsString
		return mapper.readValue(loginResponse, AuthenticationResponse::class.java)
	}

	@Test
	fun logout() {
		val token = getAuth().refreshToken
		mockMvc.perform(MockMvcRequestBuilders.post("/api/auth/logout").contentType(MediaType.APPLICATION_JSON)
			.content(mapper.writeValueAsString(LogoutRequest(token))))
			.andExpect(status().isOk)
	}

	@Test
	fun createLoanApplication() {
		val token = getAuth().authenticationToken
		doTest(CreateLoanAppRequest(
			"personal Id",
			"firstname",
			"lastname",
			Instant.ofEpochMilli(977577769000),
			"employer",
			5000.0.toBigDecimal(),
			1000.0.toBigDecimal(),
			50000.0.toBigDecimal(),
			"month"), token, LoanApplicationService.LoanStatus.APPROVED)

		doTest(CreateLoanAppRequest(
			"personal ID",
			"first",
			"last",
			Instant.ofEpochMilli(977577769000),
			"employer",
			1000.0.toBigDecimal(),
			500.0.toBigDecimal(),
			50000.0.toBigDecimal(),
			"month"), token, LoanApplicationService.LoanStatus.REJECTED)

	}

	private fun doTest(
		createLoanAppRequest: CreateLoanAppRequest,
		token: String,
		status: LoanApplicationService.LoanStatus,
	) {
		val loanResponse = performCreateLoanApplication(createLoanAppRequest, token)
			.andExpect(status().isCreated).andReturn().response.contentAsString
		val responseDto = mapper.readValue(loanResponse, LoanApplicationDto::class.java)
		assert(responseDto.status == status)
	}

	fun performCreateLoanApplication(
		createLoanAppRequest: CreateLoanAppRequest,
		authenticationToken: String,
	): ResultActions {
		val request = MockMvcRequestBuilders.post("/api/loan-application")
			.header("Authorization", "Bearer $authenticationToken")
			.contentType(MediaType.APPLICATION_JSON)
			.content(mapper.writeValueAsString(createLoanAppRequest))
		return mockMvc.perform(request)
	}

	@Test
	fun editLoanApplication() {
		val token = getAuth().authenticationToken
		val loanApplication = CreateLoanAppRequest(
			"personal Id",
			"firstname",
			"lastname",
			Instant.ofEpochMilli(977577769000),
			"employer",
			800.0.toBigDecimal(),
			200.0.toBigDecimal(),
			50000.0.toBigDecimal(),
			"month")
		val loanResponse = performCreateLoanApplication(loanApplication, token).andReturn().response.contentAsString
		val responseDto = mapper.readValue(loanResponse, LoanApplicationDto::class.java)
		val id: Long = responseDto.id
		given(loanApplicationRepository.getOne(id))
			.willReturn(LoanApplication(
				responseDto.personalId,
				responseDto.firstName,
				responseDto.lastName,
				responseDto.birthDate,
				responseDto.employer,
				responseDto.salary,
				responseDto.monthlyLiability,
				responseDto.requestedAmount,
				responseDto.requestedTerm,
				responseDto.status, id))
		val result = mockMvc.perform(MockMvcRequestBuilders.post("/api/loan-application/edit/{id}", id)
			.header("Authorization", "Bearer $token")
			.contentType(MediaType.APPLICATION_JSON)
			.content(mapper.writeValueAsString(UpdateStatusRequest(LoanApplicationService.LoanStatus.APPROVED))))
			.andExpect(status().isOk).andReturn().response.contentAsString
		val editResponse = mapper.readValue(result, LoanApplicationDto::class.java)
		assert(editResponse.status == LoanApplicationService.LoanStatus.APPROVED)
	}

	@Test
	fun getAllManualApplications() {
		val token = getAuth().authenticationToken
		val request = MockMvcRequestBuilders.get("/api/loan-application/manualApps")
			.header("Authorization", "Bearer $token")
		val result = mockMvc.perform(request)
			.andExpect(status().isOk)
			.andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON))
			.andExpect(MockMvcResultMatchers.jsonPath("$[0].status",
				`is`(LoanApplicationService.LoanStatus.MANUAL.name)))
		val response = mapper.readValue(result.andReturn().response.contentAsString, List::class.java)
		assert(response.size == 1)
	}

	@Test
	fun getAllLoanApplications() {
		val token = getAuth().authenticationToken
		val request =
			MockMvcRequestBuilders.get("/api/loan-application").param("sort", "firstName").param("direction", "ASC")
				.header("Authorization", "Bearer $token")
		val resultSorted = mockMvc.perform(request)
			.andExpect(status().isOk)
			.andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON))
		val responseSorted = mapper.readValue(resultSorted.andReturn().response.contentAsString, List::class.java)
		assert(responseSorted.size == 2)
	}

	@Test
	fun deleteLoanApplication() {
		val token = getAuth().authenticationToken
		val loanResponse = performCreateLoanApplication(CreateLoanAppRequest(
			"personal Id",
			"firstname",
			"lastname",
			Instant.ofEpochMilli(977577769000),
			"employer",
			800.0.toBigDecimal(),
			200.0.toBigDecimal(),
			50000.0.toBigDecimal(),
			"month"), token).andReturn().response.contentAsString
		val id = mapper.readValue(loanResponse, LoanApplicationDto::class.java).id
		mockMvc.perform(MockMvcRequestBuilders.delete("/api/loan-application/{id}", id)
			.header("Authorization", "Bearer $token"))
			.andExpect(status().isOk)
	}
}